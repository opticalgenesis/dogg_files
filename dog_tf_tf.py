import pandas as pd
import numpy as np
import tensorflow as tf
import os
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D
from tensorflow.keras.applications import NASNetMobile
from tensorflow.keras.applications.nasnet import preprocess_input
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam

base_model = NASNetMobile(weights='imagenet', include_top=False, input_shape=(224, 224, 3))
x = base_model.output
x = GlobalAveragePooling2D()(x)
x = Dense(1024, activation='relu')(x)
x = Dense(1024, activation='relu')(x)
x = Dense(512, activation='relu')(x)
preds = Dense(120, activation='softmax')(x)

model = Model(inputs=base_model.input, outputs=preds)

# for layer in base_model.layers[:20]:
#     layer.trainable = False
# for layers in base_model.layers[20:]:
#     layer.trainable = True

for layer in base_model.layers:
    layer.trainable = False

train_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)
train_generator = train_datagen.flow_from_directory(
    'train',
    target_size=(224, 224),
    color_mode='rgb',
    batch_size=32,
    class_mode='categorical',
    shuffle=True
)

valid_generator = train_datagen.flow_from_directory(
    'validation',
    target_size=(224, 224),
    color_mode='rgb',
    batch_size=1,
    class_mode='categorical',
    shuffle=True
)


test_datagen = ImageDataGenerator(rescale=1./255)
test_generator = test_datagen.flow_from_directory(
    'test',
    target_size=(224, 224),
    color_mode='rgb',
    batch_size=6,
    class_mode='categorical',
    shuffle=False
)

tb = tf.keras.callbacks.TensorBoard(log_dir='./logs_mnet')
es = tf.keras.callbacks.EarlyStopping(monitor='loss', min_delta=0.01, patience=2, verbose=1, mode='auto', baseline=None, restore_best_weights=True)

model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

if not os.path.exists('./weights'):
    model.fit_generator(generator=train_generator, steps_per_epoch=(train_generator.n // train_generator.batch_size), epochs=100, callbacks=[tb, es], validation_data=valid_generator, validation_steps=(train_generator.n // train_generator.batch_size))

print(model.evaluate_generator(valid_generator, (train_generator.n // train_generator.batch_size)))

test_generator.reset()
pred = model.predict_generator(test_generator, verbose=1, steps=(test_generator.n // test_generator.batch_size))
pred_class_indices = np.argmax(pred, axis=1)

labels = (train_generator.class_indices)
labels = dict((v, k) for k,v in labels.items())
preds_ = [labels[k] for k in pred_class_indices]

filenames = test_generator.filenames
res = pd.DataFrame({"Filename": filenames,
                    "Predictions": preds_})

res.to_csv('res.csv', index=False)

model.save_weights('./weights/nasnet_weights')
model.save('nasnet_.h5')
