import tensorflow as tf
from tensorflow.keras import layers

from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import numpy as np
import pandas as pd
import os

import operator

def init_model():
    model = tf.keras.Sequential()
    model.add(layers.Conv2D(32, (3, 3), input_shape=(250, 250, 1), activation='relu'))
    model.add(layers.MaxPooling2D(pool_size=(2,2)))

    model.add(layers.Conv2D(32, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))

    model.add(layers.Conv2D(256, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(pool_size=(2,2)))

    model.add(layers.Flatten())
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(120, activation='softmax'))
    return model

def init_generators():
    t_d_generator = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.05,
        zoom_range=0.3,
        horizontal_flip=False,
        vertical_flip=True
    )

    test_d_generator = ImageDataGenerator(
        rescale=1./255
    )

    train_generator = t_d_generator.flow_from_directory(
        'train',
        target_size=(250, 250),
        batch_size=64,
        class_mode='categorical',
        shuffle=True,
        color_mode='grayscale'
    )

    validation_generator = t_d_generator.flow_from_directory(
        'validation',
        target_size=(250, 250),
        batch_size=1,
        class_mode='categorical',
        shuffle=True,
        color_mode='grayscale'
    )

    test_generator = test_d_generator.flow_from_directory(
        'test',
        target_size=(250, 250),
        batch_size=6,
        color_mode='grayscale',
        class_mode=None,
        shuffle=False
    )

    return train_generator, validation_generator, test_generator

def compile_model(model):
    model.compile(
        optimizer='rmsprop',
        loss=tf.keras.losses.categorical_crossentropy,
        metrics=['categorical_accuracy']
    )

def evaluate(model, g, steps):
    print(model.evaluate_generator(generator=g, steps=steps))

def load(model):
    # USES TF CHECKPOINT, INCOMPATIBLE WITH VANILLA KERAS
    model.load_weights('./weights/initial_tf_weights')
    compile_model(model)

def new_weights(model, t_gen, t_steps, v_gen, v_steps):
    compile_model(model)
    tb = tf.keras.callbacks.TensorBoard(log_dir='./logs_new')
    model.fit_generator(
        t_gen, 
        epochs=150, 
        steps_per_epoch=t_steps,
        validation_data=v_gen,
        validation_steps=v_steps,
        callbacks=[tb],
        use_multiprocessing=True)
    os.mkdir('./weights')
    model.save_weights('./weights/initial_tf_weights')
    model.save('tf_model.h5')

def get_prediction(test_gen, model, t_steps, tr_gen):
    test_gen.reset()
    pred = model.predict_generator(test_gen, verbose=1, steps=t_steps)
    pred_class_indices = np.argmax(pred, axis=1)

    labels = (tr_gen.class_indices)
    labels = dict((v, k) for k,v in labels.items())
    preds = [labels[k] for k in pred_class_indices]

    filenames = test_gen.filenames
    res = pd.DataFrame({"Filename": filenames,
                        "Predictions": preds})

    res.to_csv('res.csv', index=False)


def predict_o(t_g, model, tr_g):
    i = image.load_img('/home/josh/Pictures/o.jpg', target_size=(250, 250))
    x = image.img_to_array(i)
    x = np.expand_dims(x, axis=0)

    pred = model.predict_classes(x)
    p_c_i = np.argmax(pred)

    labels = (tr_g.class_indices)
    labels = dict((v, k) for k,v in labels.items())
    
    _, v = max(enumerate(labels), key=operator.itemgetter(1))

    print(v)


model = init_model()
(tr_g, v_g, t_g) = init_generators()

STEP_SIZE_TRAIN = tr_g.n // tr_g.batch_size
STEP_SIZE_VALID = v_g.n // v_g.batch_size
STEP_SIZE_TEST = t_g.n // t_g.batch_size

if os.path.exists('./weights'):
    load(model)
else:
    new_weights(model, tr_g, STEP_SIZE_TRAIN, v_g, STEP_SIZE_VALID)

#evaluate(model, v_g, STEP_SIZE_VALID)
#get_prediction(t_g, model, STEP_SIZE_TEST, tr_g)
predict_o(t_g, model, tr_g)
