import os
import pandas as pd
import glob
from shutil import copyfile

labels_and_data = pd.read_csv('labels.csv')

for _, r in labels_and_data.iterrows():
    l = r['breed']
    if not os.path.exists('validation/'+l):
        os.makedirs('validation/'+l)

a = [x[0] for x in os.walk('./train')]
a.pop(0)
print(a)

abs = []

for x in a:
    imgs = glob.glob(x+'/*')
    for i in list(range(round((len(imgs) * 0.2)))):
        abs.append(os.path.abspath(imgs[i]))

for y in abs:
    print(y)

mod = []

for d in abs:
    mod.append(d.replace('train', 'validation'))

for e in mod:
    print(e)

for i, r in enumerate(abs):
    copyfile(r, mod[i])
