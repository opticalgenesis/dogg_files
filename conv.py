import argparse
import tensorflow as tf

from tensorflow import lite

parser = argparse.ArgumentParser()
parser.add_argument('--input_model', help='keras h5 model to convert')
parser.add_argument('--output_file', help='name of output file')
args = parser.parse_args()

model = args.input_model

conv = lite.TFLiteConverter.from_keras_model_file(model)
tf_model = conv.convert()
open(args.output_file, 'wb').write(tf_model)